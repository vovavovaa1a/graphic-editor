import { Injectable } from '@angular/core';
import { BehaviorSubject, from, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { map, tap } from 'rxjs/operators';
import { IUser } from '../model/iuser';
import { ILogin } from '../model/i-login';
import { DrawingElementService } from '../drawing-element-service/drawing-element-service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _user$ = new BehaviorSubject<IUser>(null);

  redirectUrl = '';

  constructor(private afAuth: AngularFireAuth, private router: Router) {
    this.afAuth.authState
      .pipe(
        map(fullUser => {
          if (fullUser) {
            return { uid: fullUser.uid, email: fullUser.email };
          }

          return null;
        })
      )
      .subscribe(user => {
        this._user$.next(user);

        if (this.redirectUrl) {
          this.router.navigate([this.redirectUrl]);
          this.redirectUrl = '';
        }
      });
  }

  getUser$(): Observable<IUser> {
    return this._user$.asObservable();
  }

  get isLoggedIn$(): Observable<boolean> {
    return this._user$.asObservable().pipe(map(user => !!user));
  }

  login(email: string, password: string): Observable<any> {
    return from(this.afAuth.auth.signInWithEmailAndPassword(email, password));
  }

  signUp({ email, password }: ILogin): Observable<any> {
    return from(this.afAuth.auth.createUserWithEmailAndPassword(email, password)).pipe(
      tap(() => {
        if (this.redirectUrl) {
          this.router.navigate([this.redirectUrl]);
          this.redirectUrl = '';
        }
      })
    );
  }

  logout() {
    this.afAuth.auth.signOut().then(() => {});
  }
}
