import { Routes } from '@angular/router';
import { RegistrationFormComponent } from '../user-data/user-data-components/registration-form/registration-form.component';
import { GraphicEditorComponent } from '../graphic-editor/graphic-editor.component';
import { NotFoundComponent } from '../user-data/not-found/not-found.component';
import { SavedDrawingsComponent } from '../user-data/user-data-components/saved-drawings/saved-drawings.component';
import { AuthGuard } from '../auth/auth.guard';
import { LoginComponent } from '../user-data/user-data-components/login/login.component';

export const appRoutes: Routes = [
  {
    path: 'registration',
    component: RegistrationFormComponent
  },
  {
    path: '',
    component: GraphicEditorComponent
  },
  {
    path: 'saving',
    canActivate: [AuthGuard],
    component: SavedDrawingsComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];
