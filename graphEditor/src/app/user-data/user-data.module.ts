import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistrationFormComponent } from './user-data-components/registration-form/registration-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NotFoundComponent } from './not-found/not-found.component';
import { SavedDrawingsComponent } from './user-data-components/saved-drawings/saved-drawings.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [RegistrationFormComponent, NotFoundComponent, SavedDrawingsComponent],
  imports: [CommonModule, RouterModule.forChild([]), ReactiveFormsModule],
  exports: [RegistrationFormComponent]
})
export class UserDataModule {}
