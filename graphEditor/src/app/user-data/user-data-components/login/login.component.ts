import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../auth/auth.service';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

const USER_NOT_FOUND_ERROR = 'auth/user-not-found';
const DEFAULT = 'DEFAULT';
const ERROR_MESSAGE = {
  [DEFAULT]: 'Неизвестная ошибка. Попробуйте позже',
  [USER_NOT_FOUND_ERROR]: 'Неверные имя пользователя и пароль'
};

@Component({
  selector: 'tfs-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  errorMessage = '';

  constructor(private authService: AuthService, private router: Router, private formBuilder: FormBuilder) {}

  get email(): AbstractControl {
    return this.form.get('email');
  }

  get password(): AbstractControl {
    return this.form.get('password');
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      email: ['test@test.test', [Validators.required, Validators.email]],
      password: ['test123', [Validators.required, Validators.minLength(6)]]
    });
  }

  submit() {
    if (this.form.invalid) {
      return;
    }

    this.errorMessage = '';

    const { email, password } = this.form.value;

    this.authService.login(email, password).subscribe(
      () => {
        this.form.reset();
        this.router.navigateByUrl('/');
      },
      error => {
        const code = error && error.code ? error.code : null;

        this.errorMessage = ERROR_MESSAGE[code] || ERROR_MESSAGE.DEFAULT;
      }
    );
  }
  signUp() {
    this.router.navigateByUrl('/registration');
  }
}
