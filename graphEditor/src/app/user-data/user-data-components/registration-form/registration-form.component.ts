import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../auth/auth.service';
import { Router } from '@angular/router';

//********************************************************************************Валидаторы
const USER_NOT_FOUND_ERROR = 'auth/user-not-found';
const DEFAULT = 'DEFAULT';
const ERROR_MESSAGE = {
  [DEFAULT]: 'Неизвестная ошибка. Попробуйте позже',
  [USER_NOT_FOUND_ERROR]: 'Неверные имя пользователя и пароль'
};

const agreeValidator = (control: FormControl) => {
  const condition = !!control.value;
  if (!condition) {
    return { agreeValidator: 'Нужно обязательно нажать' };
  }
  return null;
};

const passwordValidator = (control: FormControl) => {
  let passwordRegex: RegExp;
  let condition;

  // passwordRegex = /(?=.*[0-9])(?=.*[!?.,_+\-*/=])(?=.*[a-z])(?=.*[A-Z])/g;
  passwordRegex = /.*[a-z]/g;
  if (control.value != null) {
    condition = control.value.match(passwordRegex);
  }
  if (!condition) {
    return { agreeValidator: 'Нужно обязательно нажать' };
  }
  return null;
};

// ************************************************************************

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.css']
})
export class RegistrationFormComponent implements OnInit {
  form: FormGroup;
  errorMessage;

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) {}

  ngOnInit() {
    this.form = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(80)]],
      surname: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(80)]],
      email: ['', [Validators.pattern('.{3,}@.{2,}\\..{2,}'), Validators.required, Validators.maxLength(100)]],
      password: ['', [Validators.required, passwordValidator]],
      confirmPassword: ['', Validators.required],
      agree: ['', [Validators.required, agreeValidator]]
    });
  }

  onSubmit() {
    if (this.form.invalid) {
      return;
    }
    let temp = {
      email: '',
      password: ''
    };
    this.errorMessage = '';
    temp.email = this.form.value.email;
    temp.password = this.form.value.password;

    this.authService.signUp(temp).subscribe(
      () => {
        this.form.reset();
        this.router.navigateByUrl('/');
      },
      error => {
        const code = error && error.code ? error.code : null;

        this.errorMessage = ERROR_MESSAGE[code] || ERROR_MESSAGE.DEFAULT;
      }
    );
  }
}
