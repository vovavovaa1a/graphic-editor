import { Component, OnDestroy, OnInit } from '@angular/core';
import { IDrawing } from '../../../model/i-user-data';
import { DrawingElementService } from '../../../drawing-element-service/drawing-element-service';
import { DrawingHttpService } from '../../../drawing-element-service/drawing-http-service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-saved-drawings',
  templateUrl: './saved-drawings.component.html',
  styleUrls: ['./saved-drawings.component.css']
})
export class SavedDrawingsComponent implements OnInit, OnDestroy {
  chooseContent;
  SVG_Saved;
  drawings: IDrawing[] = [];
  requestExplaining;
  errorNetWorkMassage = '';
  Destroy$ = new Subject();

  constructor(private mainElem: DrawingElementService, private drawingHttpService: DrawingHttpService) {}

  ngOnInit() {
    this.chooseContent = this.drawings[0];
    this.SVG_Saved = document.getElementById('SVG_Saved');
    this.getDrawings();

    this.drawingHttpService.connectionState$.pipe(takeUntil(this.Destroy$)).subscribe(value => {
      if (value) {
        this.errorNetWorkMassage = '';
        this.getDrawings();
      }
    });
  }

  ngOnDestroy() {
    this.Destroy$.next();
  }

  onDeleteClick(i) {
    this.drawingHttpService.deleteDrawing(i).subscribe(() => {
      this.getDrawings();
    });
    if (this.drawings.length == 1) {
      this.drawings = [];
    }
    this.SVG_Saved.innerHTML = '';
    this.chooseContent = null;
  }

  onClick(el) {
    this.SVG_Saved.innerHTML = el.content;
    this.chooseContent = el;
  }

  download() {
    this.mainElem.downloadElement(this.chooseContent);
  }

  getDrawings() {
    this.requestExplaining = true;
    if (this.drawingHttpService.connectionState === true) {
      this.drawingHttpService.getDrawings().subscribe(drawings => {
        this.drawings = drawings;
        this.requestExplaining = false;
      });
    } else {
      this.errorNetWorkMassage = 'Ожидается подклчение к сети';
    }
  }
}
