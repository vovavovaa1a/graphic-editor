export interface IDrawing {
  id?: string;
  name: string;
  content: string;
}
