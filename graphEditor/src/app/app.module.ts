import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { UserDataModule } from './user-data/user-data.module';
import { GraphicEditorModule } from './graphic-editor/graphic-editor.module';
import { RxTxService } from './graphic-editor/GE-service/graphic-editor-service';
import { GraphicEditorComponent } from './graphic-editor/graphic-editor.component';
import { RouterModule } from '@angular/router';
import { appRoutes } from './routing/app.routing';
import { HttpClientModule } from '@angular/common/http';
import { LoginModule } from './user-data/user-data-components/login/login.module';
import { environment } from '../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';

@NgModule({
  declarations: [AppComponent, GraphicEditorComponent],
  imports: [
    UserDataModule,
    LoginModule,
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    GraphicEditorModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule
  ],
  providers: [RxTxService],
  bootstrap: [AppComponent]
})
export class AppModule {}
