import { Component, OnInit } from '@angular/core';
import { RxTxService } from '../../GE-service/graphic-editor-service';

@Component({
  selector: 'app-button-field-ge',
  templateUrl: './button-field-ge.component.html',
  styleUrls: ['./button-field-ge.component.css']
})
export class ButtonFieldGEComponent implements OnInit {
  constructor(private _service: RxTxService) {}

  ngOnInit() {
    // ******************************************* Форма изменения сетки
    let input = document.querySelector('#gridInput');
    let inputVal = <HTMLInputElement>document.getElementById('gridInput');
    let g = changeInput.bind(this);

    function changeInput() {
      if (Number(inputVal.value) >= 5) {
        this._service.gridCountReceiver$.next(inputVal.value);
      } else {
        this._service.gridCountReceiver$.next(1);
      }
    }

    input.addEventListener('change', g);
    // ******************************************************************
    // ******************************************* Форма прилипания сетки (Checkbox)
    let inputStick = document.querySelector('#gridInputStick');
    let inputValStick = <HTMLInputElement>document.getElementById('gridInputStick');
    let gStick = changeInputStick.bind(this);

    function changeInputStick() {
      this._service.gridStick$.next(inputValStick.checked);
    }

    inputStick.addEventListener('change', gStick);
    // ******************************************************************
  }

  onClickReset() {
    this._service.envokeForm$.next('None');
  }

  onClickLine() {
    this._service.envokeForm$.next('Line');
  }

  onClickCircle() {
    this._service.envokeForm$.next('Circle');
  }

  onClickRect() {
    this._service.envokeForm$.next('Rect');
  }

  onClickPolygon() {
    this._service.envokeForm$.next('Polygon');
  }

  onClickBCurve() {
    this._service.envokeForm$.next('BCurve');
  }
}
