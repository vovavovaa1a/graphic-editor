import { Component, OnDestroy, OnInit } from '@angular/core';
import { RxTxService } from '../../../GE-service/graphic-editor-service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnInit, OnDestroy {
  constructor(private _service: RxTxService) {}
  Destroy$ = new Subject();

  ngOnInit() {
    let footer = document.querySelector('footer');
    footer.style.setProperty('--div-gridSize', 20 + 'px');

    this._service.gridCountReceiver$.pipe(takeUntil(this.Destroy$)).subscribe(value => {
      if (Number(value) === 1) {
        footer.style.setProperty('--div-gridSize', '0');
      } else {
        footer.style.setProperty('--div-gridSize', value + 'px');
      }
    });
  }

  ngOnDestroy() {
    this.Destroy$.next();
  }
}
