import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { RxTxService } from '../../GE-service/graphic-editor-service';
import { fromEvent, Subject, timer } from 'rxjs';
import { auditTime, takeUntil } from 'rxjs/operators';
import { DrawingElementService } from '../../../drawing-element-service/drawing-element-service';

@Component({
  selector: 'app-graphic-field',
  templateUrl: './graphic-field.component.html',
  styleUrls: ['./graphic-field.component.css']
})
export class GraphicFieldComponent implements OnInit, OnDestroy, AfterViewInit {
  positionTempFlow$: Subject<any> = new Subject();
  Destroy$ = new Subject();

  Pos = 'X:Y';
  SVG_Writer;
  posX;
  posY;
  gridValue = 20;
  gridStick = false;
  cursorElement;
  SVG_Container;
  footer;
  zoomSize = 1;

  constructor(private mainElem: DrawingElementService, private _service: RxTxService) {}

  ngOnInit() {
    this.cursorElement = document.getElementById('cursorElement');
    this.SVG_Writer = document.getElementById('SVG_Writer');
    this.SVG_Container = document.querySelector('.ge-fields');
    this.footer = document.querySelector('footer');

    this.positionTempFlow$
      .pipe(
        auditTime(40),
        takeUntil(this.Destroy$)
      )
      .subscribe(value => {
        this.Pos = 'X:Y (' + this.posX + ':' + this.posY + ')';
        this.posX = this.gridRound(
          (value.clientX - Math.floor(this.SVG_Writer.getBoundingClientRect().x)) / this.zoomSize
        );
        this.posY = this.gridRound(
          (value.clientY - Math.floor(this.SVG_Writer.getBoundingClientRect().y)) / this.zoomSize
        );

        this._service.positionFlow$.next([this.posX, this.posY]);

        this.cursorElement.style.left = this.posX - 12 + 'px';
        this.cursorElement.style.top = this.posY - 12 + 'px';
      });

    this._service.gridStick$.pipe(takeUntil(this.Destroy$)).subscribe(value => {
      this.gridStick = value;
    });

    this._service.gridCountReceiver$.pipe(takeUntil(this.Destroy$)).subscribe(value => {
      this.gridValue = value;
    });

    fromEvent(document, 'keydown')
      .pipe(takeUntil(this.Destroy$))
      .subscribe((e: KeyboardEvent) => {
        if (e.keyCode === 90 && e.ctrlKey && document.getElementById('SVG_Writer').lastChild) {
          document.getElementById('SVG_Writer').removeChild(document.getElementById('SVG_Writer').lastChild);
          this.mainElem.InnerSVG_ToDrawingContent();
        }
        if (e.keyCode === 90) {
          e.preventDefault ? e.preventDefault() : (e.returnValue = false);
        }
      });
    //*********************************Колксико для зума
    let onWheelBind = onWheel.bind(this);

    if (this.SVG_Container.addEventListener) {
      if ('onwheel' in this.SVG_Container) {
        // IE9+, FF17+, Ch31+
        this.SVG_Container.addEventListener('wheel', onWheelBind);
      }
    }

    function onWheel(e) {
      e = e || window.event;

      if (e.ctrlKey) {
        let delta = e.wheelDelta;
        e.preventDefault ? e.preventDefault() : (e.returnValue = false);
        if (e.ctrlKey && true) {
          if (!(delta < 0 && this.zoomSize < 0.15)) {
            this.zoomSize += delta / 800;
          }
          document.getElementById('zoom-field').style.transform = `scale(${this.zoomSize.toFixed(2)})`;
          this.footer.style.backgroundImage =
            `linear-gradient(rgba(100, 100, 100, 0.33) ${(1 / this.zoomSize).toFixed(2)}px, transparent 1px),` +
            `linear-gradient(90deg, rgba(100, 100, 100, 0.54) ${(1 / this.zoomSize).toFixed(2)}px, transparent 1px)`;
        }
      }
    }
  }

  ngAfterViewInit() {
    this.mainElem.set_SVG_Writer();
    this.mainElem.updateSVG();
  }

  ngOnDestroy() {
    this.Destroy$.next();
  }

  onClick() {
    this._service.graphFieldClickEvent(this.gridRound(this.posX), this.gridRound(this.posY));
  }

  onMouseMove(event) {
    this.positionTempFlow$.next(event);
  }

  private gridRound(value) {
    return this.gridStick ? Math.round(value / this.gridValue) * this.gridValue : Math.round(value);
  }
}
