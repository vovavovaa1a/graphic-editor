import { Component, OnDestroy, OnInit } from '@angular/core';
import { DrawingElementService } from '../../../drawing-element-service/drawing-element-service';
import { Observable, Subject, timer } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthService } from '../../../auth/auth.service';
import { IUser } from '../../../model/iuser';

@Component({
  selector: 'app-user-field',
  templateUrl: './user-field.component.html',
  styleUrls: ['./user-field.component.css']
})
export class UserFieldComponent implements OnInit, OnDestroy {
  createElForm = false;
  Destroy$ = new Subject();
  activeLoader = false;
  projectName;
  user$: Observable<IUser>;

  constructor(private mainElem: DrawingElementService, private authService: AuthService) {}

  ngOnInit() {
    this.projectName = this.mainElem.currentDrawing == undefined ? ' ---' : this.mainElem.currentDrawing.name;
    timer(10000, 10000)
      .pipe(takeUntil(this.Destroy$))
      .subscribe(() => {
        this.projectName = this.mainElem.currentDrawing == undefined ? ' ---' : this.mainElem.currentDrawing.name;
        this.mainElem.autoSaveEl();
        this.activeLoader = true;
        setTimeout(() => {
          this.activeLoader = false;
        }, 3000);
      });
    this.user$ = this.authService.getUser$();
  }

  ngOnDestroy() {
    this.Destroy$.next();
  }

  onSaveClick() {
    this.mainElem.saveContent();
  }

  onClear() {
    this.mainElem.clearContent();
  }

  createEl() {
    this.createElForm = true;
  }

  createElSubmit() {
    let qwe: any = document.querySelector('#createElInput');
    this.mainElem.createElemByName(qwe.value);
    this.createElForm = false;
    this.projectName = this.mainElem.currentDrawing == undefined ? ' ---' : this.mainElem.currentDrawing.name;
  }

  cancelElSubmit() {
    this.createElForm = false;
  }
  onLogout() {
    this.authService.logout();
  }
}
