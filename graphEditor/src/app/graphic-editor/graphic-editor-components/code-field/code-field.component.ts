import { Component, OnDestroy, OnInit } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { RxTxService } from '../../GE-service/graphic-editor-service';
import { fromEvent, Subject } from 'rxjs';
import { DrawingElementService } from '../../../drawing-element-service/drawing-element-service';

@Component({
  selector: 'app-code-field',
  templateUrl: './code-field.component.html',
  styleUrls: ['./code-field.component.css']
})
export class CodeFieldComponent implements OnInit, OnDestroy {
  Destroy$ = new Subject();
  SVG_Writer;
  SVG_CodeWriter;

  constructor(private _service: RxTxService, private mainElem: DrawingElementService) {}

  ngOnInit() {
    this.SVG_Writer = document.getElementById('SVG_Writer');
    this.SVG_CodeWriter = document.getElementById('svgCodeField');

    this._service.updateCodeField$.pipe(takeUntil(this.Destroy$)).subscribe(() => {
      this.SVG_CodeWriter.value = this.SVG_Writer.innerHTML;
    });
  }

  changeCodeField() {
    this.mainElem.NewDrawingContent(this.SVG_CodeWriter.value);
  }

  ngOnDestroy() {
    this.Destroy$.next();
  }
}
