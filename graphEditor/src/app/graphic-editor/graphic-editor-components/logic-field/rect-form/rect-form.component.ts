import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { fromEvent, Subject } from 'rxjs';
import { RxTxService } from '../../../GE-service/graphic-editor-service';
import { takeUntil } from 'rxjs/operators';
import { DrawingElementService } from '../../../../drawing-element-service/drawing-element-service';

@Component({
  selector: 'app-rect-form',
  templateUrl: './rect-form.component.html',
  styleUrls: ['./rect-form.component.css']
})
export class RectFormComponent implements OnInit, OnDestroy, AfterViewInit {
  form: FormGroup;
  propArr = ['X1, px:', 'Y2, px:', 'X2, px:', 'Y2, px:', 'Stroke-width, px:', 'X-radius', 'Y-radius'];

  pos = [, , , , 2, 2, 2]; //["X1, px:", "Y2, px:", "X2, px:", "Y2, px:", "Stroke-width, px:"]
  sizeRect = []; //[X, Y, Width, Height]
  eventPoint = true;
  SVG_Temp;
  Destroy$ = new Subject();
  colorRectOutside;
  colorRectInside;
  switchFillInputValue;
  colorRectInsideTemp;

  constructor(
    private mainElem: DrawingElementService,
    private _service: RxTxService,
    private ref: ChangeDetectorRef,
    private formBuilder: FormBuilder
  ) {
    fromEvent(document, 'keydown')
      .pipe(takeUntil(this.Destroy$))
      .subscribe((e: KeyboardEvent) => {
        if (e.keyCode === 13 && this.form.valid) {
          this.onSubmit();
        } else if (e.keyCode === 27) {
          this.rectClear();
        }
      });

    this._service.positionFlow$.pipe(takeUntil(this.Destroy$)).subscribe(value => {
      this.doPositionChanging(value);
    });

    this._service.graphFieldClickEvent$.pipe(takeUntil(this.Destroy$)).subscribe(value => {
      this.doPositionChanging(value);
      this.eventPoint = !this.eventPoint;
      this.ClickDataIndication();
      if (this.form.valid) {
        this.calculateSizeRect();
        this.onSubmit();
      }
    });
  }

  ngOnInit() {
    this.formBuild();
    this.colorRectOutside = '#000000';
    this.colorRectInside = '#000000';
    this.colorRectInsideTemp = 'none';
  }

  ngAfterViewInit() {
    this.SVG_Temp = document.getElementById('SVG_Temp');
    this.form.value.strokeWidth = 2;
    this.form.value.rx = 2;
    this.form.value.ry = 2;
    this.rectClear();
  }

  ngOnDestroy() {
    this.Destroy$.next();
    this.SVG_Temp.innerHTML = '';
  }

  private doPositionChanging(value) {
    if (this.eventPoint) {
      this.pos[0] = value[0];
      this.pos[1] = value[1];
    } else {
      this.pos[2] = value[0];
      this.pos[3] = value[1];
      this.calculateSizeRect();
      this.onInput();
    }
  }

  formBuild() {
    this.form = this.formBuilder.group({
      x: ['', [Validators.required, Validators.min(0), Validators.max(5000), Validators.pattern('[0-9]*')]],
      y: ['', [Validators.required, Validators.min(0), Validators.max(5000), Validators.pattern('[0-9]*')]],
      width: ['', [Validators.required, Validators.min(0), Validators.max(5000), Validators.pattern('[0-9]*')]],
      height: ['', [Validators.required, Validators.min(0), Validators.max(5000), Validators.pattern('[0-9]*')]],
      strokeWidth: ['', [Validators.required, Validators.min(0), Validators.max(5000), Validators.pattern('[0-9]*')]],
      rx: ['', [Validators.required, Validators.min(0), Validators.max(5000), Validators.pattern('[0-9]*')]],
      ry: ['', [Validators.required, Validators.min(0), Validators.max(5000), Validators.pattern('[0-9]*')]],
      colorIn: [''],
      colorOut: ['']
    });
  }

  onSubmit() {
    this.mainElem.AddToDrawingContent(`<rect x=\"${this.sizeRect[0]}\" y=\"${this.sizeRect[1]}\" 
            width=\"${this.sizeRect[2]}\" height=\"${this.sizeRect[3]}\"
            stroke=\"${this.colorRectOutside}\" stroke-width=\"${this.pos[4]}\" 
            rx=\"${this.pos[5]}\" ry=\"${this.pos[6]}\" 
            fill=\"${this.colorRectInsideTemp}\"></rect>`);
    this.rectClear();
  }

  onInput() {
    this.calculateSizeRect();
    if (this.form.valid && this.pos[1]) {
      this.SVG_Temp.innerHTML = `<rect x=\"${this.sizeRect[0]}\" y=\"${this.sizeRect[1]}\"
            width=\"${this.sizeRect[2]}\" height=\"${this.sizeRect[3]}\"
            stroke=\"${this.colorRectOutside}\" stroke-width=\"${this.pos[4]}\" 
             rx=\"${this.pos[5]}\" ry=\"${this.pos[6]}\" 
             fill=\"${this.colorRectInsideTemp}\"></rect>`;
    }
  }

  private fillChange() {
    this.switchFillInputValue = <HTMLInputElement>document.getElementById('switchFill');
    if (this.switchFillInputValue.checked) {
      this.colorRectInsideTemp = this.colorRectInside;
    } else {
      this.colorRectInsideTemp = 'none';
    }
    if (this.form.valid) {
      this.onInput();
    }
  }

  ClickDataIndication() {
    document.getElementById('x__Rect').style.border = this.eventPoint ? 'ridge 2px red' : 'solid 0px red';
    document.getElementById('y__Rect').style.border = this.eventPoint ? 'ridge 2px red' : 'solid 0px red';
    document.getElementById('w__Rect').style.border = !this.eventPoint ? 'ridge 2px red' : 'solid 0px red';
    document.getElementById('h__Rect').style.border = !this.eventPoint ? 'ridge 2px red' : 'solid 0px red';
  }

  rectClear() {
    this.eventPoint = true;
    this.pos = [, , , , this.form.value.strokeWidth, this.form.value.rx, this.form.value.ry];

    this.SVG_Temp.innerHTML = '';
    this.ClickDataIndication();
  }

  private calculateSizeRect() {
    this.sizeRect[0] = Math.min(this.pos[0], this.pos[2]);
    this.sizeRect[1] = Math.min(this.pos[1], this.pos[3]);
    this.sizeRect[2] = Math.abs(this.pos[0] - this.pos[2]);
    this.sizeRect[3] = Math.abs(this.pos[1] - this.pos[3]);
  }
}
