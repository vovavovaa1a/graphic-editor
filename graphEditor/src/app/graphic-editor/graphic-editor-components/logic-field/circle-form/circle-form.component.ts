import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RxTxService } from '../../../GE-service/graphic-editor-service';
import { filter, takeUntil } from 'rxjs/operators';
import { fromEvent, Subject } from 'rxjs';
import { DrawingElementService } from '../../../../drawing-element-service/drawing-element-service';

@Component({
  selector: 'app-circle-form',
  templateUrl: './circle-form.component.html',
  styleUrls: ['./circle-form.component.css']
})
export class CircleFormComponent implements OnInit, OnDestroy, AfterViewInit {
  form: FormGroup;
  propArr = ['X, px:', 'Y, px:', 'R, px:', 'Width, px:'];

  pos = [, , , 2];
  eventPoint = true;
  SVG_Temp;
  Destroy$ = new Subject();
  colorCircleOutside;
  colorCircleInside;
  switchFillInputValue;
  colorCircleInsideTemp;

  constructor(
    private mainElem: DrawingElementService,
    private _service: RxTxService,
    private ref: ChangeDetectorRef,
    private formBuilder: FormBuilder
  ) {
    fromEvent(document, 'keydown')
      .pipe(takeUntil(this.Destroy$))
      .subscribe((e: KeyboardEvent) => {
        if (e.keyCode === 13 && this.form.valid) {
          this.onSubmit();
        } else if (e.keyCode === 27) {
          this.circleClear();
        }
      });

    this._service.positionFlow$.pipe(takeUntil(this.Destroy$)).subscribe(value => {
      this.doPositionChanging(value);
    });

    this._service.graphFieldClickEvent$.pipe(takeUntil(this.Destroy$)).subscribe(value => {
      this.doPositionChanging(value);
      this.eventPoint = !this.eventPoint;
      this.ClickDataIndication();
      if (this.form.valid) {
        this.onSubmit();
      }
    });
  }

  ngOnInit() {
    this.formBuild();
    this.colorCircleOutside = '#000000';
    this.colorCircleInside = '#000000';
    this.colorCircleInsideTemp = 'none';
  }

  ngAfterViewInit() {
    this.SVG_Temp = document.getElementById('SVG_Temp');
    this.form.value.width = 2;
    this.circleClear();
  }

  ngOnDestroy() {
    this.Destroy$.next();
    this.SVG_Temp.innerHTML = '';
  }

  private doPositionChanging(value) {
    if (this.eventPoint) {
      this.pos[0] = value[0];
      this.pos[1] = value[1];
    } else {
      this.pos[2] = Math.round(Math.sqrt(Math.pow(this.pos[0] - value[0], 2) + Math.pow(this.pos[1] - value[1], 2)));
      this.onInput();
    }
  }

  formBuild() {
    this.form = this.formBuilder.group({
      x: ['', [Validators.required, Validators.min(0), Validators.max(5000), Validators.pattern('[0-9]*')]],
      y: ['', [Validators.required, Validators.min(0), Validators.max(5000), Validators.pattern('[0-9]*')]],
      r: ['', [Validators.required, Validators.min(0), Validators.max(5000), Validators.pattern('[0-9]*')]],
      width: ['', [Validators.required, Validators.min(0), Validators.max(5000), Validators.pattern('[0-9]*')]],
      colorIn: [''],
      colorOut: ['']
    });
  }

  onSubmit() {
    this.mainElem.AddToDrawingContent(`<circle cx=\"${this.pos[0]}\" cy=\"${this.pos[1]}\" r=\"${this.pos[2]}\"
            stroke=\"${this.colorCircleOutside}\" stroke-width=\"${this.pos[3]}\" fill=\"${
      this.colorCircleInsideTemp
    }\"></circle>`);
    this.circleClear();
  }

  onInput() {
    if (this.form.valid && this.pos[1]) {
      this.SVG_Temp.innerHTML = `<circle cx=\"${this.pos[0]}\" cy=\"${this.pos[1]}\" r=\"${this.pos[2]}\"
            stroke=\"${this.colorCircleOutside}\" stroke-width=\"${this.pos[3]}\" fill=\"${
        this.colorCircleInsideTemp
      }\"></circle>`;
    }
  }

  fillChange() {
    this.switchFillInputValue = <HTMLInputElement>document.getElementById('switchFill');
    if (this.switchFillInputValue.checked) {
      this.colorCircleInsideTemp = this.colorCircleInside;
    } else {
      this.colorCircleInsideTemp = 'none';
    }
    if (this.form.valid) {
      this.onInput();
    }
  }

  ClickDataIndication() {
    document.getElementById('x__Circle').style.border = this.eventPoint ? 'ridge 2px red' : 'solid 0px red';
    document.getElementById('y__Circle').style.border = this.eventPoint ? 'ridge 2px red' : 'solid 0px red';
    document.getElementById('r__Circle').style.border = !this.eventPoint ? 'ridge 2px red' : 'solid 0px red';
  }

  circleClear() {
    this.eventPoint = true;
    this.pos = [, , , this.form.value.width];
    this.SVG_Temp.innerHTML = '';
    this.ClickDataIndication();
  }
}
