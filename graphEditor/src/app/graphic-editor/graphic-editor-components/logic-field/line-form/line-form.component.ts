import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RxTxService } from '../../../GE-service/graphic-editor-service';
import { takeUntil } from 'rxjs/operators';
import { fromEvent, Subject } from 'rxjs';
import { DrawingElementService } from '../../../../drawing-element-service/drawing-element-service';

@Component({
  selector: 'app-line-form',
  templateUrl: './line-form.component.html',
  styleUrls: ['./line-form.component.css']
})
export class LineFormComponent implements OnInit, OnDestroy, AfterViewInit {
  form: FormGroup;
  propArr = ['X1, px:', 'Y1, px:', 'X2, px:', 'Y2, px:', 'Width, px:'];

  pos = [, , , , 2];
  eventPoint;
  SVG_Temp;
  Destroy$ = new Subject();
  colorLineInput;

  constructor(
    private mainElem: DrawingElementService,
    private _service: RxTxService,
    private formBuilder: FormBuilder
  ) {
    fromEvent(document, 'keydown')
      .pipe(takeUntil(this.Destroy$))
      .subscribe((e: KeyboardEvent) => {
        if (e.keyCode === 13 && this.form.valid) {
          this.onSubmit();
        } else if (e.keyCode === 27) {
          this.lineClear();
        }
      });

    this._service.positionFlow$.pipe(takeUntil(this.Destroy$)).subscribe(value => {
      this.doPositionChanging(value);
    });

    this._service.graphFieldClickEvent$.pipe(takeUntil(this.Destroy$)).subscribe(value => {
      this.doPositionChanging(value);
      this.eventPoint = !this.eventPoint;
      this.ClickDataIndication();
      if (this.form.valid) {
        this.onSubmit();
      }
    });
  }

  ngOnInit() {
    this.formBuild();
    this.colorLineInput = '#000000';
  }

  ngAfterViewInit() {
    this.SVG_Temp = document.getElementById('SVG_Temp');
    this.form.value.width = 2;
    this.lineClear();
  }

  ngOnDestroy() {
    this.Destroy$.next();
    this.SVG_Temp.innerHTML = '';
  }

  private doPositionChanging(value) {
    if (this.eventPoint) {
      this.pos[0] = value[0];
      this.pos[1] = value[1];
    } else {
      this.pos[2] = value[0];
      this.pos[3] = value[1];
      this.onInput();
    }
  }

  formBuild() {
    this.form = this.formBuilder.group({
      x1: ['', [Validators.required, Validators.min(0), Validators.max(5000), Validators.pattern('[0-9]*')]],
      y1: ['', [Validators.required, Validators.min(0), Validators.max(5000), Validators.pattern('[0-9]*')]],
      x2: ['', [Validators.required, Validators.min(0), Validators.max(5000), Validators.pattern('[0-9]*')]],
      y2: ['', [Validators.required, Validators.min(0), Validators.max(5000), Validators.pattern('[0-9]*')]],
      width: ['', [Validators.required, Validators.min(0), Validators.max(5000), Validators.pattern('[0-9]*')]],
      color: ['']
    });
  }

  onSubmit() {
    this.SVG_Temp.innerHTML = '';

    this.mainElem.AddToDrawingContent(`<line x1=\"${this.pos[0]}\" x2=\"${this.pos[2]}\" y1=\"${this.pos[1]}\"
          y2=\"${this.pos[3]}\" stroke=\"${this.colorLineInput}\"
           stroke-width=\"${this.form.value.width}\" stroke-linecap=\"round\"></line>`);

    this.pos = [, , , , this.form.value.width];
    this.eventPoint = true;
  }

  onInput() {
    if (this.form.valid && this.pos[1]) {
      this.SVG_Temp.innerHTML = `<line x1=\"${this.form.value.x1}\" x2=\"${this.form.value.x2}\" y1=\"${
        this.form.value.y1
      }\"
            y2=\"${this.form.value.y2}\" stroke=\"${this.colorLineInput}\" stroke-width=\"${
        this.form.value.width
      }\" stroke-linecap=\"round\"></line>`;
    }
  }

  ClickDataIndication() {
    document.getElementById('x1__Line').style.border = this.eventPoint ? 'ridge 2px red' : 'solid 0px red';
    document.getElementById('x2__Line').style.border = !this.eventPoint ? 'ridge 2px red' : 'solid 0px red';
    document.getElementById('y1__Line').style.border = this.eventPoint ? 'ridge 2px red' : 'solid 0px red';
    document.getElementById('y2__Line').style.border = !this.eventPoint ? 'ridge 2px red' : 'solid 0px red';
  }

  lineClear() {
    this.eventPoint = true;
    this.pos = [, , , , 2];
    this.SVG_Temp.innerHTML = '';
    this.ClickDataIndication();
  }
}
