import { AfterViewInit, Component, OnDestroy, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { fromEvent, Subject } from "rxjs";
import { DrawingElementService } from "../../../../drawing-element-service/drawing-element-service";
import { RxTxService } from "../../../GE-service/graphic-editor-service";
import { takeUntil } from "rxjs/operators";

@Component({
  selector: "app-polygon-form",
  templateUrl: "./polygon-form.component.html",
  styleUrls: ["./polygon-form.component.css"]
})
export class PolygonFormComponent implements OnInit, OnDestroy, AfterViewInit {
  form: FormGroup;
  propArr = ["X, px:", "Y, px:", "Width, px:"];

  pos = [, , 2];
  coordinateStr = "";
  SVG_Temp;
  switchFillInputValue;
  Destroy$ = new Subject();
  colorPolygonInside;
  colorPolygonOutside;
  colorPolygonInsideTemp;

  constructor(
    private mainElem: DrawingElementService,
    private _service: RxTxService,
    private formBuilder: FormBuilder
  ) {
    fromEvent(document, "keydown")
      .pipe(takeUntil(this.Destroy$))
      .subscribe((e: KeyboardEvent) => {
        if (e.keyCode === 13 && this.form.valid) {
          this.coordinateStr =
            this.coordinateStr.slice(0, this.coordinateStr.lastIndexOf(" "));
          this.onCreatePolygon();
        } else if (e.keyCode === 27) {
          this.polygonClear();
        }
      });

    this._service.positionFlow$.pipe(takeUntil(this.Destroy$)).subscribe(value => {
      this.doPositionChanging(value);
    });

    this._service.graphFieldClickEvent$.pipe(takeUntil(this.Destroy$)).subscribe(value => {
      this.doPositionChanging(value);
      this.coordinateStr += ` ${this.pos[0]},${this.pos[1]}`;
    });
  }

  ngOnInit() {
    this.formBuild();
    this.colorPolygonInside = "#000000";
    this.colorPolygonOutside = "#000000";
    this.colorPolygonInsideTemp = "none";
  }

  ngAfterViewInit() {
    this.SVG_Temp = document.getElementById("SVG_Temp");
    this.form.value.width = 2;
    this.polygonClear();
  }

  ngOnDestroy() {
    this.Destroy$.next();
    this.SVG_Temp.innerHTML = "";
  }

  private doPositionChanging(value) {
    this.pos[0] = value[0];
    this.pos[1] = value[1];
    this.onInput();
  }

  formBuild() {
    this.form = this.formBuilder.group({
      x: ["", [Validators.required, Validators.min(0), Validators.max(5000), Validators.pattern("[0-9]*")]],
      y: ["", [Validators.required, Validators.min(0), Validators.max(5000), Validators.pattern("[0-9]*")]],
      width: ["", [Validators.required, Validators.min(0), Validators.max(5000), Validators.pattern("[0-9]*")]],
      colorIn: [""],
      colorOut: [""]
    });
  }

  onSubmit() {
    this.coordinateStr += ` ${this.pos[0]},${this.pos[1]}`;
  }

  onCreatePolygon() {
    this.mainElem.AddToDrawingContent(`<polygon points=\"${this.coordinateStr}\"
              y2=\"${this.form.value.y2}\" stroke=\"${this.colorPolygonOutside}\" stroke-width=\"${
      this.form.value.width
      }\" stroke-linecap=\"round\" fill=\"${this.colorPolygonInsideTemp}\"></polygon>`);

    this.pos = [, , this.form.value.width];
    this.SVG_Temp.innerHTML = "";
    this.coordinateStr = "";
  }

  changeCoordinateStr() {
    this.coordinateStr =
      this.coordinateStr.slice(0, this.coordinateStr.lastIndexOf(" ")) + ` ${this.pos[0]},${this.pos[1]}`;
  }

  onInput() {
    if (this.form.valid && this.pos[1]) {
      this.changeCoordinateStr();
      this.SVG_Temp.innerHTML = `<polygon points=\"${this.coordinateStr}\"
              y2=\"${this.form.value.y2}\" stroke=\"${this.colorPolygonOutside}\" stroke-width=\"${
        this.form.value.width
        }\" stroke-linecap=\"round\" fill=\"${this.colorPolygonInsideTemp}\"></polygon>`;
    }
  }

  fillChange() {
    this.switchFillInputValue = <HTMLInputElement>document.getElementById("switchPolygonFill");
    if (this.switchFillInputValue.checked) {
      this.colorPolygonInsideTemp = this.colorPolygonInside;
    } else {
      this.colorPolygonInsideTemp = "none";
    }
    if (this.form.valid) {
      this.onInput();
    }
  }

  polygonClear() {
    this.pos = [, , 2];
    this.SVG_Temp.innerHTML = "";
    this.coordinateStr = "";
  }
}
