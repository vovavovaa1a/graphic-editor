import { Component, OnDestroy } from '@angular/core';
import { RxTxService } from '../../GE-service/graphic-editor-service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-logic-field',
  templateUrl: './logic-field.component.html',
  styleUrls: ['./logic-field.component.css']
})
export class LogicFieldComponent implements OnDestroy {
  constructor(private _service: RxTxService) {
    this._service.envokeForm$.pipe(takeUntil(this.Destroy$)).subscribe(value => {
      this.rectActive = false;
      this.circleActive = false;
      this.lineActive = false;
      this.polygonActive = false;
      this.BCurveActive = false;

      switch (value) {
        case 'Line':
          this.lineActive = true;
          break;
        case 'Circle':
          this.circleActive = true;
          break;
        case 'Rect':
          this.rectActive = true;
          break;
        case 'Polygon':
          this.polygonActive = true;
          break;
        case 'BCurve':
          this.BCurveActive = true;
          break;
      }
    });
  }

  Destroy$ = new Subject();
  rectActive = false;
  circleActive = false;
  lineActive = false;
  polygonActive = false;
  BCurveActive = false;

  ngOnDestroy() {
    this.Destroy$.next();
  }
}
