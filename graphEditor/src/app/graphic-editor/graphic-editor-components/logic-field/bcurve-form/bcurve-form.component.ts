import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RxTxService } from '../../../GE-service/graphic-editor-service';
import { takeUntil } from 'rxjs/operators';
import { fromEvent, Subject } from 'rxjs';
import { DrawingElementService } from '../../../../drawing-element-service/drawing-element-service';
import { easeBounceOut } from 'd3-ease';

@Component({
  selector: 'app-bcurve-form',
  templateUrl: './bcurve-form.component.html',
  styleUrls: ['./bcurve-form.component.css']
})
export class BCurveFormComponent implements OnInit, OnDestroy, AfterViewInit {
  form: FormGroup;
  propArr = ['X1, px:', 'Y1, px:', 'R, px:', 'Width, px:', 'Color:'];
  properties = [, , , , , , , , 2];
  eventClickMain = true;
  eventClickProp = true;
  Destroy$ = new Subject();
  SVG_Temp;
  commandString: string;
  incidenString: string;
  colorLineInput;
  configStatus = false;

  constructor(
    private mainElem: DrawingElementService,
    private formBuilder: FormBuilder,
    private _service: RxTxService
  ) {
    fromEvent(document, 'keydown')
      .pipe(takeUntil(this.Destroy$))
      .subscribe((e: KeyboardEvent) => {
        if (e.keyCode === 13 && this.form.valid) {
          this.onSubmit();
        } else if (e.keyCode === 27) {
          this.svgClear();
        }
        if (e.keyCode === 17 && this.form.value.x1 !== undefined) {
          this.configStatus = true;
        }
      });
    fromEvent(document, 'keyup')
      .pipe(takeUntil(this.Destroy$))
      .subscribe((e: KeyboardEvent) => {
        if (e.keyCode === 17) {
          this.configStatus = false;
        }
      });

    this._service.positionFlow$.pipe(takeUntil(this.Destroy$)).subscribe(value => {
      this.doPositionChanging(value);
    });

    this._service.graphFieldClickEvent$.pipe(takeUntil(this.Destroy$)).subscribe(value => {
      this.doPositionChanging(value);
      this.eventClickMain = !this.configStatus ? !this.eventClickMain : this.eventClickMain;
      this.eventClickProp = this.configStatus ? !this.eventClickProp : this.eventClickProp;
      // if (this.form.valid) {
      //   this.onSubmit();
      // }
    });
  }

  ngOnInit() {
    this.formBuild();
    this.colorLineInput = '#000000';
  }
  ngAfterViewInit() {
    this.SVG_Temp = document.getElementById('SVG_Temp');
    this.form.value.width = 2;
    this.svgClear();
  }

  ngOnDestroy() {
    this.Destroy$.next();
    this.SVG_Temp.innerHTML = '';
  }

  onSubmit() {
    this.mainElem.AddToDrawingContent(this.commandString);
    this.SVG_Temp.innerHTML = '';
    this.properties = [, , , , , , , , this.form.value.width];
    this.eventClickMain = true;
    this.eventClickProp = true;
  }

  onInput() {
    if (this.form.valid && this.properties[1]) {
      this.curveConstructor();
      this.SVG_Temp.innerHTML = this.commandString + this.incidenString;
    }
  }

  curveConstructor() {
    this.incidenString = '';
    if (this.form.value.x3 !== undefined) {
      if (this.form.value.x4 !== undefined) {
        this.commandString = `<path
        d=\"M${this.form.value.x1} ${this.form.value.y1}
        C ${this.form.value.x3} ${this.form.value.y3}, ${this.form.value.x4} ${this.form.value.y4},
            ${this.form.value.x2} ${this.form.value.y2}\"
        stroke=\"${this.colorLineInput}\" fill="transparent" stroke-width=\"${this.form.value.width}\"
        />`;
        this.generateLine(this.form.value.x1, this.form.value.y1, this.form.value.x3, this.form.value.y3, 'red');
        this.generateLine(this.form.value.x2, this.form.value.y2, this.form.value.x4, this.form.value.y4, 'red');
      } else {
        this.commandString = `<path
        d=\"M${this.form.value.x1} ${this.form.value.y1}
        Q ${this.form.value.x3} ${this.form.value.y3}, ${this.form.value.x2} ${this.form.value.y2}\"
        stroke=\"${this.colorLineInput}\" fill="transparent" stroke-width=\"${this.form.value.width}\"
        />`;
        this.generateLine(this.form.value.x1, this.form.value.y1, this.form.value.x3, this.form.value.y3, 'red');
        this.generateLine(this.form.value.x2, this.form.value.y2, this.form.value.x3, this.form.value.y3, 'red');
      }
    } else {
      if (!this.eventClickMain) {
        this.generateLine(this.form.value.x1, this.form.value.y1, this.form.value.x2, this.form.value.y2, 'black');
      } else {
        this.generateLine(this.form.value.x1, this.form.value.y1, this.form.value.x3, this.form.value.y3, 'red');
      }
    }
  }
  generateLine(x1, y1, x2, y2, color) {
    const width = color === 'red' ? '1px' : this.form.value.width;
    this.incidenString += `<line x1=\"${x1}\"
    x2=\"${x2}\" y1=\"${y1}\"
    y2=\"${y2}\"
    stroke=\"${color}\" stroke-width=\"${width}\"
    stroke-linecap=\"round\"></line>`;
  }
  private doPositionChanging(value) {
    this.ClickDataIndication();
    if (!this.configStatus) {
      if (this.eventClickMain) {
        this.properties[0] = value[0];
        this.properties[1] = value[1];
      } else {
        this.properties[2] = value[0];
        this.properties[3] = value[1];
        this.onInput();
      }
    } else {
      if (this.eventClickProp) {
        this.properties[4] = value[0];
        this.properties[5] = value[1];
        this.onInput();
      } else {
        this.properties[6] = value[0];
        this.properties[7] = value[1];
        this.onInput();
      }
    }
  }

  formBuild() {
    this.form = this.formBuilder.group({
      x1: ['', [Validators.required, Validators.min(0), Validators.max(5000), Validators.pattern('[0-9]*')]],
      y1: ['', [Validators.required, Validators.min(0), Validators.max(5000), Validators.pattern('[0-9]*')]],
      x2: ['', [Validators.required, Validators.min(0), Validators.max(5000), Validators.pattern('[0-9]*')]],
      y2: ['', [Validators.required, Validators.min(0), Validators.max(5000), Validators.pattern('[0-9]*')]],
      x3: ['', [Validators.min(0), Validators.max(5000), Validators.pattern('[0-9]*')]],
      y3: ['', [Validators.min(0), Validators.max(5000), Validators.pattern('[0-9]*')]],
      x4: ['', [Validators.min(0), Validators.max(5000), Validators.pattern('[0-9]*')]],
      y4: ['', [Validators.min(0), Validators.max(5000), Validators.pattern('[0-9]*')]],
      width: ['', [Validators.required, Validators.min(0), Validators.max(5000), Validators.pattern('[0-9]*')]],
      colorOut: ['']
    });
  }
  ClickDataIndication() {
    document.getElementById('x1__BCurve').style.border =
      this.eventClickMain && !this.configStatus ? 'ridge 2px yellow' : 'solid 0px yellow';
    document.getElementById('y1__BCurve').style.border =
      this.eventClickMain && !this.configStatus ? 'ridge 2px yellow' : 'solid 0px yellow';
    document.getElementById('x2__BCurve').style.border =
      !this.eventClickMain && !this.configStatus ? 'ridge 2px yellow' : 'solid 0px yellow';
    document.getElementById('y2__BCurve').style.border =
      !this.eventClickMain && !this.configStatus ? 'ridge 2px yellow' : 'solid 0px yellow';

    document.getElementById('x3__BCurve').style.border =
      this.eventClickProp && this.configStatus ? 'ridge 2px yellow' : 'solid 0px yellow';
    document.getElementById('y3__BCurve').style.border =
      this.eventClickProp && this.configStatus ? 'ridge 2px yellow' : 'solid 0px yellow';
    document.getElementById('x4__BCurve').style.border =
      !this.eventClickProp && this.configStatus ? 'ridge 2px yellow' : 'solid 0px yellow';
    document.getElementById('y4__BCurve').style.border =
      !this.eventClickProp && this.configStatus ? 'ridge 2px yellow' : 'solid 0px yellow';
  }
  svgClear() {
    this.eventClickMain = true;
    this.eventClickProp = true;
    this.properties = [, , , , , , , , 2];
    this.SVG_Temp.innerHTML = '';
    this.ClickDataIndication();
  }
}
