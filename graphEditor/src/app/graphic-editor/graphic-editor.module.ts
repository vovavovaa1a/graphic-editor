import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GraphicFieldComponent } from './graphic-editor-components/graphic-field/graphic-field.component';
import { LogicFieldComponent } from './graphic-editor-components/logic-field/logic-field.component';
import { ButtonFieldGEComponent } from './graphic-editor-components/button-field-ge/button-field-ge.component';
import { ReactiveFormsModule } from '@angular/forms';
import { LineFormComponent } from './graphic-editor-components/logic-field/line-form/line-form.component';
import { GridComponent } from './graphic-editor-components/graphic-field/grid/grid.component';
import { CircleFormComponent } from './graphic-editor-components/logic-field/circle-form/circle-form.component';
import { CodeFieldComponent } from './graphic-editor-components/code-field/code-field.component';
import { RectFormComponent } from './graphic-editor-components/logic-field/rect-form/rect-form.component';
import { UserFieldComponent } from './graphic-editor-components/user-field/user-field.component';
import { RouterModule } from '@angular/router';
import { PolygonFormComponent } from './graphic-editor-components/logic-field/polygon-form/polygon-form.component';
import { BCurveFormComponent } from './graphic-editor-components/logic-field/bcurve-form/bcurve-form.component';

@NgModule({
  declarations: [
    ButtonFieldGEComponent,
    GraphicFieldComponent,
    LogicFieldComponent,
    CodeFieldComponent,
    LineFormComponent,
    CircleFormComponent,
    GridComponent,
    RectFormComponent,
    UserFieldComponent,
    PolygonFormComponent,
    BCurveFormComponent
  ],
  imports: [CommonModule, RouterModule.forChild([]), ReactiveFormsModule],
  exports: [ButtonFieldGEComponent, UserFieldComponent, GraphicFieldComponent, LogicFieldComponent, CodeFieldComponent]
})
export class GraphicEditorModule {}
