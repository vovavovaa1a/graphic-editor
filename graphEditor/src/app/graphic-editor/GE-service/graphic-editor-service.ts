import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class RxTxService {
  envokeForm$: Subject<any> = new Subject();
  graphFieldClickEvent$: Subject<any> = new Subject();
  positionFlow$: Subject<any> = new Subject();
  gridCountReceiver$: Subject<any> = new Subject();
  gridStick$: Subject<any> = new Subject();
  updateCodeField$: Subject<any> = new Subject();

  graphFieldClickEvent(posX, posY) {
    this.graphFieldClickEvent$.next([posX, posY]);
  }

  updateCodeField() {
    this.updateCodeField$.next();
  }
}
