import { Injectable } from '@angular/core';
import { IDrawing } from '../model/i-user-data';
import { DrawingHttpService } from './drawing-http-service';
import { RxTxService } from '../graphic-editor/GE-service/graphic-editor-service';
import { combineAll } from 'rxjs/operators';

let options = {
  year: 'numeric',
  month: 'long',
  day: 'numeric',
  hour: 'numeric',
  minute: 'numeric'
};

@Injectable({
  providedIn: 'root'
})
export class DrawingElementService {
  drawing: IDrawing;
  currentDrawing: IDrawing; // Текущий выбранный элемент, с которым ведется работа
  connectionState: boolean = false;
  errorNetWorkMassage = '';

  SVG_Writer = document.getElementById('SVG_Writer');

  constructor(private drawingHttpService: DrawingHttpService, private _service: RxTxService) {
    this.drawing = {
      name: 'Автосохранение - ' + new Date().toLocaleString('ru', options),
      content: ''
    };

    this.drawingHttpService.connectionState$.subscribe((value: boolean) => {
      if (value) {
        this.connectionState = value;
        this.errorNetWorkMassage = '';
      }
    });
  }

  //***************************************************************************************

  set_SVG_Writer() {
    this.SVG_Writer = document.getElementById('SVG_Writer');
  }

  downloadElement(el) {
    this.currentDrawing = el;
    this.drawing.content = el.content;
    this._service.updateCodeField();
    this.updateSVG();
  }

  createElemByName(drawingElContent) {
    this.currentDrawing = { name: drawingElContent, content: this.SVG_Writer.innerHTML };
  }

  AddToDrawingContent(content: string) {
    this.drawing.content += content;
    this.AddToSVG(content);
  }

  NewDrawingContent(content) {
    this.drawing.content = content;
    this.SVG_Writer.innerHTML = content;
  }

  AddToSVG(content) {
    this.SVG_Writer.innerHTML += content;
    this._service.updateCodeField();
  }

  updateSVG() {
    this.SVG_Writer.innerHTML = this.drawing.content;
    this._service.updateCodeField();
  }

  saveContent() {
    if (this.connectionState === true) {
      if (this.currentDrawing !== undefined) {
        this.currentDrawing.content = this.drawing.content;
        if (this.currentDrawing.id) {
          this.drawingHttpService.changeDrawing(this.currentDrawing).subscribe();
        } else {
          this.drawingHttpService.addDrawing(this.currentDrawing).subscribe(id => {
            this.currentDrawing.id = id;
          });
        }
      }
      this.autoSaveEl();
    }
  }

  autoSaveEl() {
    if (this.drawingHttpService.getUID() !== undefined) {
      if (this.connectionState === true) {
        this.drawing.name = 'Автосохранение - ' + new Date().toLocaleString('ru', options);
        console.log(this.drawing.name);
        if (this.drawing.id) {
          this.drawingHttpService.changeDrawing(this.drawing).subscribe(() => {});
        } else {
          this.drawingHttpService.addDrawing(this.drawing).subscribe(id => {
            this.drawing.id = id;
          });
        }
      }
    }
  }

  clearContent() {
    this.drawing.content = '';
    this.SVG_Writer.innerHTML = '';
    this._service.updateCodeField();
  }

  InnerSVG_ToDrawingContent() {
    this.drawing.content = this.SVG_Writer.innerHTML;
  }
}
