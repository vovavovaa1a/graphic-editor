import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { fromEvent, Observable, Subject } from 'rxjs';
import { IDrawing } from '../model/i-user-data';
import { filter, map } from 'rxjs/operators';
import { AuthService } from '../auth/auth.service';

const BASE_URL = 'https://graph-editor-f4e09.firebaseio.com/';
let uid;

interface IDrawingsDto {
  [id: string]: IDrawing;
}

@Injectable({
  providedIn: 'root'
})
export class DrawingHttpService {
  connectionState$ = new Subject();
  connectionState = false;

  constructor(private http: HttpClient, private authService: AuthService) {
    //*********************************************************Проверка на наличие интернет подключения
    fromEvent(window, 'load').subscribe(() => {
      if (navigator.onLine) {
        this.connectionState = true;
        this.connectionState$.next(true);
      } else {
        this.connectionState = false;
        this.connectionState$.next(false);
      }
    });

    fromEvent(window, 'online').subscribe(() => {
      this.connectionState = true;
      this.connectionState$.next(true);
    });
    fromEvent(window, 'offline').subscribe(() => {
      this.connectionState = false;
      this.connectionState$.next(false);
    });
    //**********************************************************
  }

  getUID() {
    this.authService.getUser$().subscribe(value => {
      if (value == null) {
        return;
      }
      uid = value.uid;
    });
    return uid;
  }

  getDrawings(): Observable<IDrawing[]> {
    return this.http.get<IDrawingsDto>(`${BASE_URL}/users/${this.getUID()}/drawing.json`).pipe(
      filter(value => value != null),
      map(data => Object.entries(data).map(([id, purchase]) => ({ id, ...purchase })))
    );
  }

  addDrawing(newDrawing: IDrawing): Observable<string> {
    return this.http
      .post<{ name: string }>(`${BASE_URL}/users/${this.getUID()}/drawing.json`, newDrawing)
      .pipe(map(({ name }) => name));
  }

  changeDrawing(Drawing: IDrawing): Observable<IDrawing> {
    return this.http.put<IDrawing>(`${BASE_URL}/users/${this.getUID()}/drawing/${Drawing.id}.json`, Drawing);
  }

  deleteDrawing(Drawing: IDrawing): Observable<IDrawing> {
    return this.http.delete<IDrawing>(`${BASE_URL}/users/${this.getUID()}/drawing/${Drawing.id}.json`);
  }
}
